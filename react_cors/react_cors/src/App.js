import React, { useState } from "react";
import "./App.css";
import Button from "./components/Button";
import WeatherInfo from "./components/WeatherInfo";

function App() {
  const [weatherData, setWeatherData] = useState(null);

  const handleFetchWeather = (data) => {
    setWeatherData(data);
  };

  return (
    <div className="App">
      <h1>Приложение погоды</h1>
      <Button onFetchWeather={handleFetchWeather} />
      {weatherData && <WeatherInfo weatherData={weatherData} />}
    </div>
  );
}

export default App;
