import React, { useState } from "react";
import axios from "axios";

const Button = ({ onFetchWeather }) => {
  const handleClick = async () => {
    try {
      const response = await axios.get(process.env.REACT_APP_BACKEND_URL);
      onFetchWeather(response.data);
    } catch (error) {
      console.error("Ошибка при получении прогноза погоды:", error);
    }
  };

  return (
    <div>
      <button onClick={handleClick}>Прогноз погоды</button>
    </div>
  );
};

export default Button;
