// src/components/WeatherInfo.js
import React from "react";

const WeatherInfo = ({ weatherData }) => {
  return (
    <div>
    <h2>Информация о прогнозе погоды</h2>
    {weatherData.map((forecast, index) => (
      <div key={index}>
        <p>Дата: {forecast.date}</p>
        <p>Температура: {forecast.temperature}°C</p>
        <p>Сводка: {forecast.summary}</p>
      </div>
    ))}
  </div>
  );
};

export default WeatherInfo;
